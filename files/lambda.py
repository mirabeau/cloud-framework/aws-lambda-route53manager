#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import boto3
import json
import logging
from pprint import pformat

'''DOCUMENTATION

Lambda function to manage route53 entries through a lambda call.
I.e.
Create or update the localhost.mydomain. A record recordset to point to 127.0.0.1
aws lambda invoke --function-name Route53Manager --payload '{"DomainName": "localhost.mydomain.", "Value": "127.0.0.1", "HostedZoneId": "zone1234", "RecordType": "A",  "Action": "UPSERT"}' /dev/stdout --region eu-west-1

HostedZoneId, RecordType, and Action are required fields.

Supported RecordTypes with required fields:
- A
  DomainName  - the record name to alter
  Value       - address to set to the record
- CNAME
  DomainName  - the record name to alter
  Value       - entry to set to the record

If you want to create an alias record, you'll need to add an AliasHostedZoneId as well as an AliasTarget to the payload.
The default TTL is set to 300 seconds but can be supplied if you want something else.

Action can be one of:
 - CREATE
 - DELETE
 - UPSERT
(see https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/route53.html )

TODO: Support other records such as PTR and MX

'''

logger = logging.getLogger()
logger.setLevel(logging.INFO)
logging.basicConfig(level=logging.INFO, format='%(levelname)-8s %(message)s',)
# Silence boto3 logging:
logging.getLogger('boto3').setLevel(logging.WARNING)
logging.getLogger('botocore').setLevel(logging.WARNING)


def setRoute53Record(zone, recordtype, action, data):
    logging.info("setRoute53Record called for doing [{}] to zone[{}], recordtype[{}] with data[{}]".format(action, zone, recordtype, pformat(data)))
    action = action.upper()
    ttl = 300
    if recordtype == 'A':
        try:
            domainname = data['DomainName']
            value      = data['Value']
        except Exception as e:
            logging.error('Invalid input: for A record we need both a DomainName and Value (IpAddress) [{}]'.format(pformat(e)))
            raise UserWarning('Invalid input: for A record we need both a DomainName and Value (IpAddress) [{}]'.format(pformat(e)))
    elif recordtype == 'CNAME':
        try:
            domainname = data['DomainName']
            value      = data['Value']
        except Exception as e:
            logging.error('Invalid input: for CNAME record we need both a DomainName and Value (target address) [{}]'.format(pformat(e)))
            raise UserWarning('Invalid input: for CNAME record we need both a DomainName and Value (target address) [{}]'.format(pformat(e)))
    else:
        logging.error('Invalid input, unknown "RecordType" -    can be either A or CNAME currently, got [{}]'.format(recordtype))
        raise UserWarning('Invalid input, unknown "RecordType" -    can be either A or CNAME currently, got [{}]'.format(recordtype))

    if 'ttl' in data:
        ttl = data['ttl']

    changes = {
        'Changes': [{
            'Action': action,
            'ResourceRecordSet': {
                'Name': domainname,
                'Type': recordtype,
                'ResourceRecords': [{'Value': value}],
                'TTL': int(ttl)
            }
        }]
    }
    if 'AliasHostedZoneId' and 'AliasTarget' in data:
        logging.info("aliasTarget in request to HostedZoneId[{}] with AliasTarget[{}]".format(data['AliasHostedZoneId'], data['AliasTarget']))
        changes['Changes'][0]['ResourceRecordSet']['AliasTarget'] = {
            'HostedZoneId': data['AliasHostedZoneId'],
            'DNSName': data['AliasTarget'],
            'EvaluateTargetHealth': False
        }

    try:
        route53 = boto3.client('route53')
        logging.debug("Calling changeResourceRecordSets on zone [{}] with changes[{}]".format(zone, pformat(changes)))
        res = route53.change_resource_record_sets(
            HostedZoneId=zone,
            ChangeBatch=changes
        )
    except Exception as e:
        logging.error('Route53 changeResourceRecordSets failed: [{}]'.format(pformat(e)))
        raise UserWarning('Route53 changeResourceRecordSets failed: [{}]'.format(pformat(e)))

    # Got a "An error occurred during JSON serialization of response: datetime.datetime(2018, 9, 18, 9, 59, 44, 25000, tzinfo=tzlocal()) is not JSON serializable" when returning res directly...
    logging.info("Result was: {}".format(pformat(res)))
    if res is None or 'ChangeInfo' not in res:
        {'success': False}
    return {'success': True}


def lambda_handler(event, context):
    res = None
    if 'RecordType' not in event:
        raise UserWarning('Invalid input, expected "RecordType" -    can be either A or CNAME currently.')
    if 'HostedZoneId' not in event:
        raise UserWarning('Invalid input, expected "HostedZoneId" - should match your target hosted zone.')
    if 'Action' not in event:
        raise UserWarning('Invalid input, expected "Action" - either CREATE, DELETE or UPSERT.')

    hzid     = event['HostedZoneId']
    rtype    = event['RecordType']
    action = event['Action']
    return setRoute53Record(hzid, rtype, action, event)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='AWS Route53 Manager')
    parser.add_argument('--action', metavar='action', default='UPSERT', help='action - can be CREATE, UPSERT or DELETE')
    parser.add_argument('--hostedzone', metavar='hostedzone', help='hosted zone to edit')
    parser.add_argument('--recordtype', metavar='recordtype', help='record type, either A or CNAME')
    parser.add_argument('--domain', metavar='domain', help='Target record domain name to create')
    parser.add_argument('--value', metavar='value', help='Target value for the record')
    parser.add_argument('--ttl', metavar='ttl', default=300, help='Time to live for record')
    args = parser.parse_args()
    data = {
        'DomainName': args.domain,
        'Value': args.value,
    }
    if args.ttl:
        data['ttl'] = args.ttl
    setRoute53Record(args.hostedzone, args.recordtype, args.action, data)
