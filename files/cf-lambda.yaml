---
AWSTemplateFormatVersion: '2010-09-09'
Description: Creates Route53 Manager lambda function and resources
Parameters:
  S3Bucket:
    Description: S3 bucket name where lambda functions can be found
    Type       : String
  S3Path:
    Description: Lambda zip file for lambda function
    Type       : String
  LogExpiration:
    Description: Log Expiration in days for this lambda
    Type       : Number
    Default    : 7

Resources:
  ####################################################################
  #
  # Lambda roles
  #
  ####################################################################

  LambdaRole:
    Type: AWS::IAM::Role
    Properties:
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
        - arn:aws:iam::aws:policy/AmazonRoute53FullAccess
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - lambda.amazonaws.com
            Action:
              - sts:AssumeRole

  LambdaInvokePolicy:
    Type: AWS::IAM::ManagedPolicy
    Properties:
      ManagedPolicyName: !Sub "${AWS::Region}-LambdaRoute53ManagerInvokePolicy"
      Description: "Allows invocation of Route53Manager Lambda Function"
      PolicyDocument:
        Version: '2012-10-17'
        Statement:
          - Effect: Allow
            Action: lambda:InvokeFunction
            Resource: !GetAtt Lambda.Arn

  ####################################################################
  #
  # Lambda function
  #
  ####################################################################
  Lambda:
    Type: AWS::Lambda::Function
    Properties:
      FunctionName: Route53Manager
      Description: "Manage Route53 HostedZone records through lambda invocation"
      Handler: index.lambda_handler
      MemorySize: 128
      Role: !GetAtt LambdaRole.Arn
      Runtime: python3.7
      Timeout: 300
      Code:
        S3Bucket: !Ref S3Bucket
        S3Key: !Sub "lambda/${S3Path}"

  LambdaLogGroup:
    Type: "AWS::Logs::LogGroup"
    Properties:
      RetentionInDays: !Ref LogExpiration
      LogGroupName: !Join ["", ["/aws/lambda/", !Ref Lambda]]

Outputs:
  LambdaARN:
    Value: !GetAtt Lambda.Arn
    Export:
      Name: !Sub "${AWS::StackName}-LambdaARN"
  LambdaInvokePolicyARN:
    Value: !Ref LambdaInvokePolicy
    Export:
      Name: !Sub "${AWS::StackName}-LambdaInvokePolicyARN"
